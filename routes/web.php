<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/Admin/index', 'AdminController@index')->name('Admin/home');

Route::get('/Employee/AddEmployee', 'EmployeeController@AddEmployee')->name('Admin/AddEmployee');

Route::get('/Employee/SetPassword', 'EmployeeController@SetPassword')->name('Admin/SetPassword');

Route::post('/Employee/SetPassword', 'EmployeeController@SavePassword')->name('Admin/SetPassword');

Route::post('/Employee/SaveEmployee', 'EmployeeController@SaveEmployee')->name('Employee/AddEmployee');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/Employee/AttendanceRecord', 'EmployeeController@AttendanceRecord')->name('Employee/AttendanceRecord');

Route::post('/Employee/GetAttendanceRecord', 'EmployeeController@GetAttendanceRecord')
->name('Employee/AttendanceRecord');

Route::get('/Employee/LeaveRecord', 'EmployeeController@LeaveRecord')->name('Employee/LeaveRecord');

Route::get('/Admin/LeaveSettings', 'AdminController@LeaveSettings')->name('Admin/LeaveSettings');


Route::post('/Admin/SaveLeaveTypes', 'AdminController@SaveLeaveTypes')->name('Admin/LeaveSettings');

Route::post('/Employee/SaveEmpLeave', 'EmployeeController@SaveEmpLeave')->name('Admin/SaveEmpLeave');
Route::get('/Admin/LeaveApproval', 'AdminController@LeaveApproval')->name('Admin/LeaveApproval');

Route::post('/Admin/ApproveLeave', 'AdminController@ApproveLeave')->name('Admin/ApproveLeave');

Route::post('/Admin/GetStaffApprovedLeave', 'AdminController@GetStaffApprovedLeave')->name('Admin/GetStaffApprovedLeave');

Route::post('/Admin/GetStaffRejectedLeave', 'AdminController@GetStaffRejectedLeave')->name('Admin/GetStaffRejectedLeave');

Route::post('/Leave/GetFilterLeaveRecord', 'LeaveController@GetFilterLeaveRecord')->name('Leave/GetFilterLeaveRecord');

Route::get('/Project/ProjectRecord', 'ProjectController@ProjectRecord')->name('/Project/ProjectRecord');

Route::post('/Project/SaveProjects', 'ProjectController@SaveProjects')->name('/Project/SaveProjects');


