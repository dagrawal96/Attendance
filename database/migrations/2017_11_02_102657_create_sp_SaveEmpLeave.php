<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSaveEmpLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_SaveEmpLeave;
        CREATE PROCEDURE sp_SaveEmpLeave(_Email VARCHAR(256),
                                         FromDate DATETIME,
                                         ToDate DATETIME,
                                         _LeaveTypeId INT,
                                         _Reason VARCHAR(1000))
        BEGIN
        DECLARE EmpId INT;
        DECLARE NoDays INT;
        DECLARE RemainingDays INT;
        DECLARE Message VARCHAR(1000);
        DECLARE IsError INT;
        DECLARE TakenLeave INT;
        SET EmpId=(SELECT EmployeeId FROM Employee WHERE Email=_Email);
        SET NoDays=(SELECT DATEDIFF(ToDate,FromDate))+1;
        SET RemainingDays=(SELECT Remaining FROM leaveentitlement WHERE LeaveTypeId=_LeaveTypeId 
                           AND EmployeeId=EmpId);
        SET TakenLeave=(SELECT Taken FROM leaveentitlement WHERE LeaveTypeId=_LeaveTypeId 
                           AND EmployeeId=EmpId);
        SET IsError=0;

        CREATE TEMPORARY TABLE tmpDates
        SELECT *
        FROM dates d
        WHERE d.Dates BETWEEN FromDate AND ToDate;

        IF(NoDays>RemainingDays)
        THEN
        SET Message="Your entitlement for this leave has been finished.Please choose Unpaid leave as Leave Type and apply for your leave again.";
        SET IsError=1;
        END IF;
        
        IF EXISTS(SELECT Date FROM leaverecord lr 
                  INNER JOIN tmpDates td ON td.Dates=lr.Date
                  WHERE lr.EmployeeId=EmpId)
        THEN
        SET Message="You have already applied for leave for the choosen date.";
        SET IsError=1;
        END IF;

        SET RemainingDays=RemainingDays-NoDays;
        SET TakenLeave=TakenLeave+NoDays;
        
        IF(IsError=0)
        THEN
        UPDATE leaveentitlement SET `Taken`=TakenLeave,`Remaining`=RemainingDays 
        WHERE `EmployeeId`=EmpId AND LeaveTypeId=_LeaveTypeId;
        
        INSERT INTO leaverecord(`EmployeeId`,`LeaveTypeId`,`Date`,`NoOfDays`,`IsApproved`,
          `Reason`)   
        SELECT EmpId,_LeaveTypeId,Dates,NoDays,0,_Reason
        FROM tmpDates td; 

        SET Message='Leave applied successfully and sent for approval';
        END IF;

        SELECT Message;

        DROP TEMPORARY TABLE tmpDates;

        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveEmpLeave";
        DB::connection()->getPdo()->exec($sql);
    }
}
