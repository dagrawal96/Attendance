<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpGetAttendanceRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_GetAttendanceRecord;
        CREATE PROCEDURE sp_GetAttendanceRecord(IN FromDate DateTime,
                                                IN ToDate DateTime,
                                                IN _Email VARCHAR(256))
        BEGIN
        DECLARE _EmployeeId INT;
        SET _EmployeeId=(SELECT EmployeeId FROM employee WHERE Email=_Email);

        CREATE TEMPORARY TABLE tmpDates
        SELECT *
        FROM employee e
        CROSS JOIN dates d
        WHERE d.Dates BETWEEN DATE(FromDate) AND DATE(ToDate)
        AND ((_IsAdmin=0 AND e.EmployeeId=_EmployeeId) OR (_IsAdmin=1));

        CREATE TEMPORARY TABLE tmpPresentAttendance
        SELECT AttendanceDate,
               MIN(DA.LoginTime) AS InTime,
               CASE WHEN MAX(DA.LogOutTime) IS NULL THEN DATE_ADD(DATE(AttendanceDate), INTERVAL 17 hour) 
               ELSE MAX(DA.LogoutTime) END AS OutTime,
               td.EmployeeId
        FROM `dailyattendance` DA
        INNER JOIN tmpDates td ON DATE(td.Dates)=Date(AttendanceDate) AND td.EmployeeId=DA.EmployeeId
        GROUP BY DATE(AttendanceDate);

        CREATE TEMPORARY TABLE tmpAttendance
        SELECT DATE(tmp.Dates) AS AttendanceDate,
               DAYNAME(Dates) AS Day,
               TIME(tp.InTime) AS InTime,
               TIME(tp.OutTime) AS OutTime,
               lr.Date As LeaveDate,
               tmp.Name AS EmpName,
               CASE WHEN tp.InTime IS NULL AND lr.Date IS NOT NULL THEN 'On Leave' 
                    WHEN tp.InTime IS NOT NULL THEN 'Present' 
                    ELSE 'Absent' 
               END AS Status
        FROM tmpDates tmp
        LEFT JOIN tmpPresentAttendance tp ON DATE(tmp.Dates)=DATE(tp.AttendanceDate)
        LEFT JOIN leaverecord lr ON lr.EmployeeId=tmp.EmployeeId AND DATE(lr.Date)=DATE(tmp.Dates) AND IsApproved=1
        ORDER BY tmp.Dates;

        SELECT * FROM tmpAttendance;

        DROP TEMPORARY TABLE tmpDates;
        DROP TEMPORARY TABLE tmpPresentAttendance;
        DROP TEMPORARY TABLE tmpAttendance;

        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_GetAttendanceRecord";
        DB::connection()->getPdo()->exec($sql);
    }
}
