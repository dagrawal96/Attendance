<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSaveAttendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
          DROP PROCEDURE IF EXISTS sp_SaveAttendance;
          CREATE PROCEDURE sp_SaveAttendance(IN _Email VARCHAR(256),
                                           IN _Name VARCHAR(256),
                                           IN _DOB TIMESTAMP,
                                           IN _Phone VARCHAR(256),
                                           IN _DesignationId INT
                                           )
          BEGIN
          DECLARE EmployeeId INT;
          DECLARE InsertEmployeeId INT;
          DECLARE Token CHAR(38);
          SET EmployeeId=(SELECT EmployeeId From employee WHERE Email=_Email);
          
          IF(EmployeeId=0)
          THEN
          INSERT INTO `employee`(`Name`, `Email`, `Phone`, `DesignationId`,`password_token`)
          VALUES(_Name,_Email,_Phone,_DesignationId,UUID());           
          END IF;

          SET InsertEmployeeId=(SELECT max(EmployeeId) FROM employee);
          SET Token=(SELECT password_token FROM employee WHERE EmployeeId=InsertEmployeeId);
          SELECT InsertEmployeeId AS EmployeeId,Token AS Token;
          END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveAttendance";
        DB::connection()->getPdo()->exec($sql);
    }
}
