<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpPopulateDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_PopulateDates;
        CREATE PROCEDURE sp_PopulateDates(IN startDate DATETIME,
                                          IN endDate DateTime)
        BEGIN
        WHILE startDate<=endDate DO
            INSERT INTO dates (Dates)
                    VALUES (startDate);  
            SET startDate = DATE_ADD(startDate,INTERVAL 1 DAY); 
            END WHILE;
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       $sql = "DROP PROCEDURE IF EXISTS sp_PopulateDates";
        DB::connection()->getPdo()->exec($sql);
    }
}
