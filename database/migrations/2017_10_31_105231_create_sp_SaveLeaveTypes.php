<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSaveLeaveTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_SaveLeaveTypes;
        CREATE PROCEDURE sp_SaveLeaveTypes(IN leaveType VARCHAR(256),
                                          IN noDaysAllowed INT)
        BEGIN
         INSERT INTO leavetypes (LeaveType,NoOfDaysAllowed)
                    VALUES (leaveType,noDaysAllowed);  
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveLeaveTypes";
        DB::connection()->getPdo()->exec($sql);
    }
}
