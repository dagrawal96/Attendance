<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpApproveRejectLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_ApproveRejectLeave;
        CREATE PROCEDURE sp_ApproveRejectLeave(IN EmployeeId INT,
                                               IN _LeaveRecordId INT,
                                               IN _Approvedby VARCHAR(256),
                                               IN IsApproved INT,
                                               IN _Reason VARCHAR(1000))
        BEGIN
        IF(IsApproved=1)
        THEN
        UPDATE leaverecord SET IsApproved=1,IsRejected=0,ApprovedBy=_ApprovedBy,ApprovalReason=_Reason WHERE LeaveRecordId=_LeaveRecordId; 
        ELSE 
        UPDATE leaverecord SET IsRejected=1,IsApproved=0,RejectedBy=_ApprovedBy,ApprovalReason=_Reason WHERE  LeaveRecordId=_LeaveRecordId;    
        END IF;     
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveEmpLeave";
        DB::connection()->getPdo()->exec($sql);
    }
}
