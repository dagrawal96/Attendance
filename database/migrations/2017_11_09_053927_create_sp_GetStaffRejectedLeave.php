<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpGetStaffRejectedLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_GetStaffRejectedLeave;
        CREATE PROCEDURE sp_GetStaffRejectedLeave()
        BEGIN
        SELECT *,lt.LeaveType,Date(lr.Date) AS Date,emp.Name
        FROM leaverecord lr
        LEFT JOIN leavetypes lt ON lt.LeaveTypeId=lr.LeaveTypeId
        LEFT JOIN Employee emp ON emp.EmployeeId=lr.EmployeeId
        WHERE lr.IsRejected=1;
        END
SQL;
        DB::connection()->getPdo()->exec($sql);    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       $sql = "DROP PROCEDURE IF EXISTS sp_GetStaffRejectedLeave";
       DB::connection()->getPdo()->exec($sql);
    }
}
