<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSaveLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $sql = <<<SQL
          DROP PROCEDURE IF EXISTS sp_SaveLogin;
          CREATE PROCEDURE sp_SaveLogin(IN _Email VARCHAR(256),
                                        IN _IsLogin INT)
          BEGIN
          DECLARE _EmployeeId INT;
          DECLARE _NoOfLogin INT;
          DECLARE _LastLoginId INT;
          SET _EmployeeId=(SELECT EmployeeId From employee WHERE Email=_Email);
         
          SELECT NoOfLogin,AttendanceId INTO _NoOfLogin, _LastLoginId 
          FROM `dailyattendance` WHERE DATE(AttendanceDate)=
          DATE(NOW()) AND EmployeeId=_EmployeeId  ORDER BY AttendanceId DESC LIMIT 1;

          SET _NoOfLogin=IFNULL(_NoOfLogin,0)+1;

          IF(_IsLogin=1)
          THEN
          INSERT INTO `dailyattendance`(`EmployeeId`, `LoginTime`, `LogoutTime`, `NoOfLogin`,
            `AttendanceDate`)
          VALUES(_EmployeeId,CASE WHEN _IsLogin=1 THEN NOW() ELSE NULL END, NULL,_NoOfLogin,NOW()); 
          ELSE
          UPDATE `dailyattendance` SET `LogoutTime`=NOW() WHERE AttendanceId=_LastLoginId;
          END IF; 

          END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveLogin";
        DB::connection()->getPdo()->exec($sql);
    }
}
