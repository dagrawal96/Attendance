<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSaveEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $sql = <<<SQL
          DROP PROCEDURE IF EXISTS sp_SaveEmployee;
          CREATE PROCEDURE sp_SaveEmployee(IN _EmployeeId INTEGER,
                                           IN _Name VARCHAR(256),
                                           IN _Email VARCHAR(256),
                                           IN _DOB TIMESTAMP,
                                           IN _Phone VARCHAR(256),
                                           IN _DesignationId INTEGER
                                           )
          BEGIN
          DECLARE InsertEmployeeId INT;
          DECLARE Token CHAR(38);
          IF(_EmployeeId=0)
          THEN
          INSERT INTO `employee`(`Name`, `Email`, `Phone`, `DesignationId`,`password_token`)
          VALUES(_Name,_Email,_Phone,_DesignationId,UUID());  
          END IF;
          SET InsertEmployeeId=(SELECT max(EmployeeId) FROM employee);
          SET Token=(SELECT password_token FROM employee WHERE EmployeeId=InsertEmployeeId);

          INSERT INTO `leaveentitlement`(`EmployeeId`,`LeaveTypeId`,`Entitlement`,
            `YearId`,`Taken`,`Remaining`)
          SELECT InsertEmployeeId,LeaveTypeId,NoOfDaysAllowed,1,0,NoOfDaysAllowed
          FROM leavetypes; 

          SELECT InsertEmployeeId AS EmployeeId,Token AS Token;
          END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveEmployee";
        DB::connection()->getPdo()->exec($sql);
    }
}
