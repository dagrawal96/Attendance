<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSaveUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
          DROP PROCEDURE IF EXISTS sp_SaveUser;
          CREATE PROCEDURE sp_SaveUser(IN _Email VARCHAR(256),
                                       IN _Password VARCHAR(256))
          BEGIN
          DECLARE _Name VARCHAR(256);
          SET _Name=(SELECT `Name` FROM `employee` WHERE `Email`=_Email);
          INSERT INTO `users`(`name`, `email`, `password`,`roleid`)
          VALUES(_Name,_Email,_Password,2);           
          END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveUser";
        DB::connection()->getPdo()->exec($sql);
    }
}
