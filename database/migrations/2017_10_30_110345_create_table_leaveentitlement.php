<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeaveentitlement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaveentitlement', function (Blueprint $table) {
            $table->increments('EntitlementId');
            $table->integer('EmployeeId');
            $table->integer('LeaveTypeId');
            $table->float('Entitlement');
            $table->integer('YearId');
            $table->float('Taken');
            $table->float('Remaining');            
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaveentitlement');
    }
}
