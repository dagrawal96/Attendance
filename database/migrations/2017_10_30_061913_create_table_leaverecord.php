<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeaverecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('leaverecord', function (Blueprint $table) {
            $table->increments('LeaveRecordId');
            $table->integer('EmployeeId');
            $table->integer('LeaveTypeId');
            $table->datetime('Date');
            $table->integer('NoOfDays');
            $table->integer('IsApproved');
            $table->integer('IsRejected');
            $table->text('Reason');
            $table->text('ApprovedBy');
            $table->text('RejectedBy');
            $table->text('ApprovalReason');            
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('leaverecord');
    }
}
