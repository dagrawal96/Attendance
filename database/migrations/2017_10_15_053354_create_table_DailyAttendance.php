<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDailyAttendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dailyattendance', function (Blueprint $table) {
            $table->increments('AttendanceId');
            $table->integer('EmployeeId');
            $table->dateTime('LoginTime')->nullable();
            $table->dateTime('LogoutTime')->nullable();
            $table->integer('NoOfLogin');
            $table->dateTime('AttendanceDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dailyattendance');
    }
}
