<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpGetLeaveTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_GetLeaveTypes;
        CREATE PROCEDURE sp_GetLeaveTypes()
        BEGIN
        SELECT * FROM leavetypes;  
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_GetLeaveTypes";
        DB::connection()->getPdo()->exec($sql);
    }
}
