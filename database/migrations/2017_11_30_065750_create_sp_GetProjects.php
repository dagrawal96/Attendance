<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpGetProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_GetProjects;
        CREATE PROCEDURE sp_GetProjects()
        BEGIN
        SELECT * FROM projects;  
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_GetProjects";
        DB::connection()->getPdo()->exec($sql);
    }
}
