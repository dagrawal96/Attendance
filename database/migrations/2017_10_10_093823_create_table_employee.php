<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('employee', function (Blueprint $table) {
            $table->increments('EmployeeId');
            $table->string('Name');
            $table->string('Email');
            $table->string('Phone');
            $table->integer('DesignationId');
            $table->char('password_token', 38);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('employee');
    }
}
