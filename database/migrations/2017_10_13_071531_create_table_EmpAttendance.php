<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmpAttendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empattendance', function (Blueprint $table) {
            $table->increments('AttendanceId');
            $table->integer('EmployeeId');
            $table->string('InTime');
            $table->string('Outtime');
            $table->string('Status');
            $table->string('WorkedHours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('employee');
    }
}
