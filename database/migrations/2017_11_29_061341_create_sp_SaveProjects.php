<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpSaveProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
          DROP PROCEDURE IF EXISTS sp_SaveProjects;
          CREATE PROCEDURE sp_SaveProjects(IN _Title VARCHAR(256),
                                           IN _StartDate DATETIME,
                                           IN _EndDate DATETIME,
                                           IN _Scope VARCHAR(1000),
                                           IN _IsActive INT)
          BEGIN
          INSERT INTO `projects`(`Title`,`StartDate`, `EndDate`, `Scope`,`IsActive`)
          VALUES(_Title,_StartDate,_EndDate,_Scope,_IsActive);  
          END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "DROP PROCEDURE IF EXISTS sp_SaveProjects";
        DB::connection()->getPdo()->exec($sql);
    }
}
