<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpGetEmpLeaveRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
        DROP PROCEDURE IF EXISTS sp_GetEmpLeaveRecord;
        CREATE PROCEDURE sp_GetEmpLeaveRecord(IN _Email VARCHAR(256))
        BEGIN
        DECLARE EmpId INT;
        SET EmpId=(SELECT EmployeeId FROM Employee WHERE Email=_Email);
        SELECT *,lt.LeaveType,Date(lr.Date) AS Date FROM leaverecord lr
        LEFT JOIN leavetypes lt ON lt.LeaveTypeId=lr.LeaveTypeId
        WHERE EmployeeId=EmpId; 
        END
SQL;
        DB::connection()->getPdo()->exec($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       $sql = "DROP PROCEDURE IF EXISTS sp_GetEmpLeaveRecord";
       DB::connection()->getPdo()->exec($sql);
    }
}
