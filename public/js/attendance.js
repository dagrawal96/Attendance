
   
$(document).ready(function(){ 
 $('#txtFromDate').datepicker({
  dateFormat:'yy-mm-dd',
  maxDate: '0',
  onSelect: function(dateStr) {
    var date=$(this).datepicker('getDate');
    $('#txtToDate').datepicker('option', {minDate: date});
  }
 });
 $('#txtToDate').datepicker({
  dateFormat:'yy-mm-dd',
  maxDate: '0',
  onSelect: function(dateStr) {
    var date=$(this).datepicker('getDate');
    $('#txtFromDate').datepicker('option', {maxDate: date});
  }
 });
  $('#formAttendance').on('submit', function (e) {
   e.preventDefault();
   var FromDate=$('#txtFromDate').datepicker('getDate');
   var ToDate=$('#txtToDate').datepicker('getDate');
   var _token = $('input[name="_token"]').val();
   $.ajax({
    type: "POST",
    url: "/Employee/GetAttendanceRecord",
	  dataType:"html",
    data:{ "FromDate":$.datepicker.formatDate('yy-mm-dd', FromDate),
           "ToDate":$.datepicker.formatDate('yy-mm-dd', ToDate),
           "_token":_token
         },
    cache: false,
    success:  function(data){
		    $('#divAttendanceRecord').html(data);
     }       
    });
  });
  $('#btnSaveLeaveType').on('click',function(){
    $('#formLeaveTypes').submit();
  });
  $('#btnSaveEmpLeave').on('click',function(){
    $('#formEmpLeave').submit();
  });
  $('#btnApprove').on('click',function(){
    $('#formLeaveApproval').submit();
  });
  $('#tblApproval').on('click','.approve',function(e){
    e.preventDefault();
   var me=$(this);
   var EmployeeId=me.attr('data-employeeid');
   var LeaveRecordId=me.attr('data-leaverecordid');
   $('#txtEmployeeId').val(EmployeeId);
   $('#txtIsApproved').val(1);
   $('#txtLeaveRecordId').val(LeaveRecordId);
  });
  $('#tblApproval').on('click','.reject',function(e){ 
    e.preventDefault();
   var me=$(this);
   var EmployeeId=me.attr('data-employeeid');
   var LeaveRecordId=me.attr('data-leaverecordid');
   $('#txtEmployeeId').val(EmployeeId);
   $('#txtIsApproved').val(0);
   $('#txtLeaveRecordId').val(LeaveRecordId);
  });
  $('#ddlIsApproved').on('change',function(){
    $('#ddlFilLeaveType').trigger('change');
  });
  $('#ddlFilLeaveType').on('change',function(){
   var me=$(this);
   var LeaveTypeId=me.find('option:selected').attr('value');
   var IsApproved=$('#ddlIsApproved').find('option:selected').attr('value');
   var _token = $('input[name="_token"]').val();
   $.ajax({
    type: "POST",
    url: "/Leave/GetFilterLeaveRecord",
    dataType:"json",
    data:{ "LeaveTypeId":LeaveTypeId,
           "IsApproved":IsApproved,
           "_token":_token
         },
    cache: false,
    success:  function(data){
      var html='';
      html+='<table class="table">';
      html+='<thead>';
      html+='<th>Leave Type</th>';
      html+='<th>Date</th>';
      html+='<th>Reason</th>';
      html+='<th>Is Approved</th>';
      html+='</thead>';
      html+='<tbody>';
      if (data.length > 0) {
        $.each(data, function (index, item) {
            html+='<tr>';
            html+='<td>';
            html+=item.LeaveType;
            html+='</td>';
            html+='<td>';
            html+=item.Date;
            html+='</td>';
            html+='<td>';
            html+=item.Reason;
            html+='</td>';
            html+='<td>';
            if(item.IsApproved==1){
              html+='Yes';
            }
            else{
              html+='No';
            }            
            html+='</td>';
            html+='</tr>';   
        });

      html+='</tbody></table>';
      }
        $('#divLeaveTable').html(html);
     }       
    });
  });
  $('#liLeaveApproved').on('click',function(){
   var _token = $('input[name="_token"]').val();
   $.ajax({
    type: "POST",
    url: "/Admin/GetStaffApprovedLeave",
    dataType:"html",
    data:{ "_token":_token
         },
    cache: false,
    success:  function(data){
        $('#divLeaveApproved').html(data);
     }       
    });
  });
  $('#liLeaveRejected').on('click',function(){
   var _token = $('input[name="_token"]').val();
   $.ajax({
    type: "POST",
    url: "/Admin/GetStaffRejectedLeave",
    dataType:"html",
    data:{ "_token":_token
         },
    cache: false,
    success:  function(data){
        $('#divLeaveRejected').html(data);
    }       
    });
  });

  //// Projects
  $('#btnSaveProjects').on('click',function(){
    $('#formProjects').submit();
  });
});