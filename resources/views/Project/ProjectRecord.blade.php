@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row col-md-12">
      @include('layouts.header')
       <div class="col-md-8">
       	 <div class="panel panel-default">
       	 	<div class="panel-heading">Projects</div>
                <div class="panel-body">
                  <div class="filter col-md-12">
                     <div class="col-md-4">
                    <select class="form-control" id="ddlFilLeaveType" name="ddlFilLeaveType">
                    <option></option>
                    </select></div>
                     <div class="col-md-4">
                    <select class="form-control" id="ddlIsApproved" name="ddlIsApproved">
                      <option value="1">Approved</option>
                      <option value="0">Not Approved</option>
                    </select></div>
                     <div class="col-md-4">
                    <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#myModal">Add Projects</button></div>
                  </div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Projects</h4>
      </div>
      <form id="formProjects" method="post" action="/Project/SaveProjects">
        {{csrf_field()}}
      <div class="modal-body">      
      	<div>
      		<label class="form-label">Project Title</label>
      		<input class="form-control" type="text" name="txtTitle" 
          id="txtTitle">
      	</div>
      	<div>
      		<label class="form-label">Start Date</label>
      		<input class="form-control" type="text" name="txtFromDate" id="txtFromDate">
      	</div>
        <div>
          <label class="form-label">End Date</label>
          <input class="form-control" type="text" name="txtToDate" id="txtToDate">
        </div>
        <div>
          <label class="form-label">Scope</label>
          <textarea name="txtScope"  class="form-control" id="txtScope" rows="5" cols="40"></textarea>
        </div>
         <div>
          <label class="form-label">Is Active</label>
          <input type="checkbox" id="chkIsActive" name="chkIsActive" value="1"
          class="form-check-input">
        </div>
      </div></form>
      <div class="modal-footer">
      	<button type="submit" id="btnSaveProjects" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>      
    </div>    
  </div>

</div>
<div id="divProjectsTable">
  <table class="table">
  <thead>
    <th>Title</th>
    <th>Start Date</th>
    <th>End Date</th>
    <th>Scope</th>
    <th>Is Active</th>
    <th></th>
  </thead>
  <tbody>
  @if($result)
  @foreach($result as $re)
   <tr>
    <td><strong>{{$re['Title']}}</strong></td>
    <td>{{$re['StartDate']}}</td>
    <td>{{$re['EndDate']}}</td>
    <td>{{$re['Scope']}}</td>
    <td>{{ $re['IsActive'] ? 'Yes' : 'No' }}</td>
    <td>
      <a class="reject" href="#" data-employeeid="{{$re['ProjectId']}}">
        <span data-toggle="modal" data-target="#divApprovalModal" title="Add Member">
        Add Member</span>
      </a>
    </td>
   </tr>
  @endforeach
  @endif
   
  </tbody></table>
 </div>

</div>
</div>

</div>
</div></div>

<div id="divApprovalModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Member</h4>
        </div>        
        <div class="modal-body">   
          <form id="formLeaveApproval" method="post" action="/Admin/ApproveLeave">
          {{csrf_field()}}   
            <div>
              <input type="hidden" name="txtProjectId" id="txtProjectId">          
              <label class="form-label">Member</label>
              <div id="divMembers">
                <input type="text" name="txtMember" id="txtMember" class="form-control">
              </div>
            </div>
          </form>
        </div>       
        <div class="modal-footer">
          <button type="submit" id="btnAddMembers" class="btn btn-default" data-dismiss="modal">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>      
      </div>    
    </div>
  </div>
@endsection