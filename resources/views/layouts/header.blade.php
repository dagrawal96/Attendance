<div class="col-md-4">
    
    
            <ul>
                <li>My Profile</li>
                @if(Auth::user()->roleid==1)
                <li>
                    <a href="{{action('EmployeeController@AddEmployee')}}">Add Employee</a>
                </li>
                <li>
                    <a href="{{action('AdminController@LeaveSettings')}}">Leave Settings</a>
                </li>
                @endif
                <li>
                    <a href="{{action('EmployeeController@AttendanceRecord')}}"> My Attendance Record</a>
                </li>
                <li>
                    <a href="{{action('EmployeeController@LeaveRecord')}}">Leave</a>
                </li>
                @if(Auth::user()->roleid==1)
                 <li>
                    <a href="{{action('AdminController@LeaveApproval')}}">Leave Approval</a>
                </li>
                 @endif
                <li>
                 <a href="{{action('ProjectController@ProjectRecord')}}">Projects</a></li>
            </ul>
        </div>