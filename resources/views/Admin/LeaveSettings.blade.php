@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row col-md-12">
      @include('layouts.header')
       <div class="col-md-8">
       	 <div class="panel panel-default">
       	 	<div class="panel-heading">Leave Types</div>
                <div class="panel-body">
<button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#myModal">Add Leave Types</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Leave Types</h4>
      </div>
      <form id="formLeaveTypes" method="post" action="/Admin/SaveLeaveTypes">
        {{csrf_field()}}
      <div class="modal-body">      
      	<div>
      		<label class="form-label">Leave Type</label>
      		<input class="form-control" type="text" name="txtLeaveType" id="txtLeaveType">
      	</div>
      	<div>
      		<label class="form-label">No of Days allowed</label>
      		<input class="form-control" type="text" name="txtDays" id="txtDays">
      	</div>
      </div></form>
      <div class="modal-footer">
      	<button type="submit" id="btnSaveLeaveType" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>      
    </div>    
  </div>

</div>
  <table class="table-condensed">
  <thead>
    <th>Leave Type</th>
    <th>Days Allowed</th>
  </thead>
  <tbody>
     @if($result)
  @foreach($result as $re)
   <tr>
    <td>{{$re['LeaveType']}}</td>
    <td>{{$re['NoOfDaysAllowed']}}</td>
   </tr>
  @endforeach
  @endif
   
  </tbody></table>
 

</div>
</div>

</div>
</div></div>
@endsection