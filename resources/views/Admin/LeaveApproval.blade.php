  @extends('layouts.app')
  @section('content')

    <div class="container">
      <div class="row col-md-12">
        @include('layouts.header')     
           <div class="col-md-8">
         	   <div class="panel panel-default">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a data-toggle="tab" href="#ApprovalLeaves">Staff Leave to Approve
                    </a>
                  </li>
                  <li id="liLeaveApproved"><a data-toggle="tab" href="#LeaveApproved">Approved Leave</a></li>
                  <li id="liLeaveRejected"><a data-toggle="tab" href="#LeaveRejected">Rejected Leave</a></li>
                </ul>
              </div>
              <div class="tab-content">
               <div id="ApprovalLeaves" class="panel panel-default tab-pane fade in active">
                  <div class="panel-heading">
                   Leave Approval
                  </div>
                  <div class="panel-body">
                    <div class="filter col-md-12">
                      <div class="col-md-4">
                        <select class="form-control" id="ddlFilLeaveType" name="ddlFilLeaveType">
                          @if($result)
                          @foreach($result as $re)
                          <option value="{{$re['LeaveTypeId']}}">{{$re['LeaveType']}}</option>
                          @endforeach
                          @endif
                        </select>
                      </div>
                      <div class="col-md-4">
                        <select class="form-control" id="ddlIsApproved" name="ddlIsApproved">
                          <option value="1">Approved</option>
                          <option value="0">Not Approved</option>
                        </select>
                      </div>                    
                    </div>

                    <table id="tblApproval" class="table">
                    <thead>
                      <th>Applied By</th>
                      <th>Leave Type</th>
                      <th>Applied For</th>  
                      <th>Reason</th>    
                    </thead>
                    <tbody>
                        @if($leavesTaken)
                        @foreach($leavesTaken as $re)
                          <tr>
                            <td>{{$re['Name'] }}</td>
                            <td>{{$re['LeaveType']}}</td>
                            <td>{{$re['Date']}}</td>
                            <td>{{$re['Reason']}}</td>
                            <td>
                              <a class="approve" href="#" data-employeeid="{{$re['EmployeeId']}}" data-leaverecordid="{{$re['LeaveRecordId']}}">
                                <span class="glyphicon glyphicon-ok" data-toggle="modal" 
                                 data-target="#divApprovalModal" 
                                title="Approve"></span>
                              </a>
                              <a class="reject" href="#" data-employeeid="{{$re['EmployeeId']}}" data-leaverecordid="{{$re['LeaveRecordId']}}">
                                <span class="glyphicon glyphicon-remove" data-toggle="modal" 
                                 data-target="#divApprovalModal"  
                                title="Reject"></span></a>
                            </td>
                          </tr>
                        @endforeach
                        @endif
                    </tbody>
                  </table>
              </div>
            </div>

              <div id="LeaveApproved" class="panel panel-default tab-pane fade in">
                <div class="panel-heading">Leave Approved
                </div>
                <div id="divLeaveApproved" class="panel-body">
                </div>
            </div>
            <div id="LeaveRejected" class="panel panel-default tab-pane fade in">
                <div class="panel-heading">Leave Rejected
                </div>
                <div id="divLeaveRejected" class="panel-body">
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  <div id="divApprovalModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Approval / Rejection </h4>
        </div>        
        <div class="modal-body">   
          <form id="formLeaveApproval" method="post" action="/Admin/ApproveLeave">
          {{csrf_field()}}   
            <div>
              <input type="hidden" name="txtEmployeeId" id="txtEmployeeId">
              <input type="hidden" name="txtIsApproved" id="txtIsApproved">
              <input type="hidden" name="txtLeaveRecordId" id="txtLeaveRecordId">          
              <label class="form-label">Reason</label>
              <textarea name="txtReason" class="form-control" id="txtReason" rows="5" cols="40">
              </textarea>
            </div>
          </form>
        </div>       
        <div class="modal-footer">
          <button type="submit" id="btnApprove" class="btn btn-default" data-dismiss="modal">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>      
      </div>    
    </div>
  </div>
  @endsection