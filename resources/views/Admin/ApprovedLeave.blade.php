 <table id="tblApproval" class="table">
                    <thead>
                      <th>Applied By</th>
                      <th>Leave Type</th>
                      <th>Applied For</th>  
                      <th>Reason</th> 
                      <th>Reason For Approval / Rejection</th>    
                    </thead>
                    <tbody>
                        @if($result)
                        @foreach($result as $re)
                          <tr>
                            <td>{{$re['Name'] }}</td>
                            <td>{{$re['LeaveType']}}</td>
                            <td>{{$re['Date']}}</td>
                            <td>{{$re['Reason']}}</td>  
                            <td>{{$re['ApprovalReason']}}</td>                           
                          </tr>
                        @endforeach
                        @endif
                    </tbody>
                  </table>