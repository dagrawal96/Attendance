@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row col-md-12">
      @include('layouts.header')
       <div class="col-md-8">
       	 <div class="panel panel-default">
       	 	<div class="panel-heading">Leave Record</div>
                <div class="panel-body">
                  <div class="filter col-md-12">
                     <div class="col-md-4">
                    <select class="form-control" id="ddlFilLeaveType" name="ddlFilLeaveType">
                      @if($result)
                      @foreach($result as $re)
                      <option value="{{$re['LeaveTypeId']}}">{{$re['LeaveType']}}
                      </option>
                      @endforeach
                      @endif
                    </select></div>
                     <div class="col-md-4">
                    <select class="form-control" id="ddlIsApproved" name="ddlIsApproved">
                      <option value="1">Approved</option>
                      <option value="0">Not Approved</option>
                    </select></div>
                     <div class="col-md-4">
                    <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#myModal">Apply for Leave</button></div>
                  </div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Leave Types</h4>
      </div>
      <form id="formEmpLeave" method="post" action="/Employee/SaveEmpLeave">
        {{csrf_field()}}
      <div class="modal-body">      
      	<div>
      		<label class="form-label">Leave Type</label>
      		<select id="ddlLeaveType" name="ddlLeaveType" class="form-control"> 
              @if($result)
              @foreach($result as $re)
              <option value="{{$re['LeaveTypeId']}}">{{$re['LeaveType']}}</option>
              @endforeach
              @endif
          </select>
      	</div>
      	<div>
      		<label class="form-label">From Date</label>
      		<input class="form-control" type="text" name="txtFromDate" id="txtFromDate">
      	</div>
        <div>
          <label class="form-label">To Date</label>
          <input class="form-control" type="text" name="txtToDate" id="txtToDate">
        </div>
        <div>
          <label class="form-label">Reason</label>
          <textarea name="txtReason"  class="form-control" id="txtReason" rows="5" cols="40"></textarea>
        </div>
      </div></form>
      <div class="modal-footer">
      	<button type="submit" id="btnSaveEmpLeave" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>      
    </div>    
  </div>

</div>
<div id="divLeaveTable">
  <table class="table">
  <thead>
    <th>Leave Type</th>
    <th>Date</th>
    <th>Reason</th>
    <th>Is Approved</th>
  </thead>
  <tbody>
  @if($leavesTaken)
  @foreach($leavesTaken as $re)
   <tr>
    <td>{{$re['LeaveType']}}</td>
    <td>{{$re['Date']}}</td>
    <td>{{$re['Reason']}}</td>
    <td>{{ $re['IsApproved'] ? 'Yes' : 'No' }}</td>
   </tr>
  @endforeach
  @endif
   
  </tbody></table>
 </div>

</div>
</div>

</div>
</div></div>
@endsection