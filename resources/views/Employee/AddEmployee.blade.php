 @extends('layouts.app')
  @section('content')

<div class="container">
      <div class="row col-md-12">
        @include('layouts.header')     
           <div class="col-md-8">
         	   <div class="panel panel-default">
         	   	<div class="panel-heading">
                   Add Employee
                </div>
                <div class="panel-body">
         	   	<form method="post" action="/Employee/SaveEmployee">
	{{csrf_field()}}
	<div>
		<label class="form-label">Name</label>
		<input type="text" id="txtName" name="txtName" class="form-control">
		@if ($errors->has('txtName'))
            <span class="help-block">
                <strong>{{ $errors->first('txtName') }}</strong>
            </span>
        @endif
	</div>
	<div>
		<label class="form-label">Email</label>
		<input type="text" id="txtEmail" name="txtEmail" class="form-control">
	</div>
	<div>
		<label class="form-label">DOB</label>
		<input type="text" id="txtDOB" name="txtDOB" class="form-control">
	</div>
	<div>
		<label class="form-label">Designation</label>
		<select name="ddlDesignation" id="ddlDesignation" class="form-control">
			<option value="1">Senior Developer</option>
			<option value="2">Junior Developer</option>
			<option value="3">Senior Designer</option>
		</select>
	</div>
	<div>
	<input type="submit" value="Save" name="btnSubmit">
</div>
</form>
</div>
</div>
</div>
</div>
</div>
@endsection