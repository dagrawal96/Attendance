@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row col-md-12">
      @include('layouts.header')
        <div class="col-md-8">
            <div class="panel panel-default">
                <form id="formAttendance" method="post" action="">
                {{csrf_field()}}
                <div class="panel-heading">Attendance Record</div>
                <div class="panel-body">
                  <div class="row col-md-12 divFilter">
                    <div class="col-md-4">
                        <label class="form-label">From Date</label>
                        <input type="text" class="form-control datepicker" id="txtFromDate" name="txtFromDate">
                    </div>                
                    <div class="col-md-4">
                        <label class="form-label">To Date</label>
                        <input type="text" class="form-control datepicker" id="txtToDate" name="txtToDate">
                    </div>
                    <div class="col-md-4">
                        <input type="submit" class="btn btn-danger" 
                        id="btnGenerateReport" name="btnGenerateReport" value="Generate Report">
                    </div>
                  </div>
                  <div class="row col-md-12">
                     <div id="divAttendanceRecord"></div>
                  </div>
                 </div>
                </form>
            </div>
        </div>
       
    </div>
</div>
@endsection
