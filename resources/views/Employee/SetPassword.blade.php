<form method="post" action="/Employee/SetPassword">
	{{csrf_field()}}
	<div>
		<input type="hidden" name="txtEmail" value="{{$Email}}">
		<input type="hidden" name="txtToken" value="{{$Token}}">
		<label>Password</label>
		<input type="text" id="txtPassword" name="txtPassword">
		@if ($errors->has('txtPassword'))
            <span class="help-block">
                <strong>{{ $errors->first('txtPassword') }}</strong>
            </span>
        @endif
	</div>
	<div>
		<label>Re-type password</label>
		<input type="text" id="txtRePassword" name="txtRePassword">
		@if ($errors->has('txtRePassword'))
            <span class="help-block">
                <strong>{{ $errors->first('txtRePassword') }}</strong>
            </span>
        @endif
	</div>
	<input type="submit" value="Save" name="btnSubmit">
</form>