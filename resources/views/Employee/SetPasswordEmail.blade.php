<p style="padding:3px 20px; font-size: 15px; font-family: Arial, Sans-serif;
color: #333; margin: 0; ">
Dear {{ $Name }},
Please click on the link below to set your password. Please remember that the link will expire in 45 minutes.

http://localhost:8000/Employee/SetPassword?Email={{ $Email }}&&Token={{ $Token }}

Thank you.