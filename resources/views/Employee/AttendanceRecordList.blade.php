<table class="table">
	<thead>
		<th>Status</th>
		<th>Day</th>
		<th>Emp Name</th>
		<th>Attendance Date</th>		
		<th>In Time</th>
		<th>Out Time</th>		
	</thead>
	<tbody>
	  @if ($result)
      @foreach($result as $re)
		<tr>
			<td>
			 @if($re["Status"]=='Present')
			  <span class="bg-primary text-white">{{$re["Status"]}}</span>
			 @else
			  <span class="bg-danger text-white">{{$re["Status"]}}</span>
			 @endif
		    </td>
		    <td>{{$re["Day"]}}</td>
		    <td>{{$re["EmpName"]}}</td>
			<td>{{$re["AttendanceDate"]}}</td>			
			<td>{{$re["InTime"]}}</td>
			<td>{{$re["OutTime"]}}</td>
			
		</tr>
	  @endforeach
     @endif
	</tbody>
</table>

   
   