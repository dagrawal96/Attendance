<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;
use Auth;

class EmployeeController extends Controller
{
    public function EmployeeList(){
    	return view("Employee.EmployeeList");
    }
    public function AddEmployee(){
        return view("Employee.AddEmployee");
    }
    public function SetPassword(Request $request){
    	$Email = $request->query('Email');
    	$Token = $request->query('Token');
    	return view("Employee.SetPassword",['Email'=>$Email,'Token'=>$Token]);
    }

    //bail method is used if one validation failed does not carry out another
    public function SaveEmployee(Request $request){
    	$EmployeeId=0;
    	$Name=$request->txtName;
    	$Email=$request->txtEmail;
    	$Phone="123";
    	$DOB=$request->txtDOB;
    	$DesignationId=$request->ddlDesignation;
    	$messages = array(
		'txtName.required'=>'Required',
		'txtEmail.required'=>'Required'
	    );

	    $rules = array(
		'txtName' => 'bail|required|max:255', 
        'txtEmail' => 'required|email',
	     );

	     $this->validate($request, $rules,$messages);
	     $db = DB::connection();
         $stmt=$db->getPdo()->prepare("CALL sp_SaveEmployee(?,?,?,?,?,?)");
         $stmt->bindParam(1, $EmployeeId);
         $stmt->bindParam(2, $Name);
         $stmt->bindParam(3, $Email);
         $stmt->bindParam(4, $DOB);
         $stmt->bindParam(5, $Phone);
         $stmt->bindParam(6, $DesignationId);
         $stmt->execute();
         $result = $stmt->fetchAll();

         $data = [
        'Name' => $Name,
        'Email' => $Email,
        'Phone' =>$Phone,
        'EmployeeId'=>$result[0]['EmployeeId'],
        'Token'=>$result[0]['Token']
        ];

         Mail::send('Employee.SetPasswordEmail', $data, function ($message) use ($Email) {
          $message->from('mail@heliairnepal.com');
          $message->to($Email)->subject('Set your password');
         });
         return view('Employee.EmployeeList',['result'=>$result]);
	     
	}    
	public function SavePassword(Request $request){
        $Email=$request->txtEmail;
        $Token=$request->txtToken;
        $Password=bcrypt($request->txtPassword);

		$messages = array(
		'txtPassword.required'=>'Required',
		'txtRePassword.required'=>'Required'
	    );

	    $rules = array(
		'txtPassword' => 'bail|required|min:6', 
        'txtRePassword' => 'required',
	    );

	     $this->validate($request, $rules,$messages);

	     $db = DB::connection();
         $stmt=$db->getPdo()->prepare("CALL sp_SaveUser(?,?)");
         $stmt->bindParam(1, $Email);
         $stmt->bindParam(2, $Password);
         $stmt->execute();
         return view("Employee.SetPassword",['Email'=>$Email,'Token'=>$Token]);
	}
    public function GetAttendanceRecord(Request $request){
     $Email = Auth::user()->email;
     $FromDate=$request->FromDate;
     $ToDate=$request->ToDate;
     $RoleId=Auth::user()->roleid;
     if($RoleId==1){
        $IsAdmin=1;
     }
     else $IsAdmin=0;
     $db = DB::connection();
     $stmt=$db->getPdo()->prepare("CALL sp_GetAttendanceRecord(?,?,?,?)");
     $stmt->bindParam(1, $FromDate);
     $stmt->bindParam(2, $ToDate); 
     $stmt->bindParam(3, $Email); 
     $stmt->bindParam(4, $IsAdmin);   
     $stmt->execute();
     $result = $stmt->fetchAll();
     return view("Employee.AttendanceRecordList",['result'=>$result]);
    }

    public function AttendanceRecord(){
        return view("Employee.AttendanceRecord");
    } 
    public function LeaveRecord(){
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetLeaveTypes()");
        $stmt->execute();
        $result=$stmt->fetchAll();
        unset($stmt);
        $Email = Auth::user()->email;
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetEmpLeaveRecord(?)");
        $stmt->bindParam(1, $Email);
        $stmt->execute();
        $leavesTaken=$stmt->fetchAll();        

        return view("Employee.LeaveRecord",['result'=>$result,'leavesTaken'=>$leavesTaken]);
    }    
    public function SaveEmpLeave(Request $request){
        $Email = Auth::user()->email;
        $FromDate=$request->txtFromDate;
        $ToDate=$request->txtToDate;
        $LeaveTypeId=$request->ddlLeaveType;
        $Reason=$request->txtReason;
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_SaveEmpLeave(?,?,?,?,?)");
        $stmt->bindParam(1, $Email);
        $stmt->bindParam(2, $FromDate); 
        $stmt->bindParam(3, $ToDate);
        $stmt->bindParam(4, $LeaveTypeId);  
        $stmt->bindParam(5, $Reason);  
        $stmt->execute();
        $result=$stmt->fetchAll();
        return redirect("Employee/LeaveRecord");
    }
}
