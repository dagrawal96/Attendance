<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class LeaveController extends Controller
{
    public function GetFilterLeaveRecord(Request $request){
        $LeaveTypeId=$request->LeaveTypeId;
        $IsApproved=$request->IsApproved;
        $Email = Auth::user()->email;
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetEmpLeaveRecordByFilter(?,?,?)");
        $stmt->bindParam(1, $Email);
        $stmt->bindParam(2, $LeaveTypeId);
        $stmt->bindParam(3, $IsApproved);
        $stmt->execute();
        $leavesTaken=$stmt->fetchAll(); 
        return response()->json($leavesTaken);
    }
}
