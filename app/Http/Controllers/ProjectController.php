<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class ProjectController extends Controller
{
    public function ProjectRecord(){
    	$Email = Auth::user()->email;
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetProjects()");
        $stmt->execute();
        $result=$stmt->fetchAll();   
        return view("Project.ProjectRecord",['result'=>$result]);
    }
    public function SaveProjects(Request $request){
    	$Email = Auth::user()->email;
        $FromDate=$request->txtFromDate;
        $ToDate=$request->txtToDate;
        $Scope=$request->txtScope;
        $Title=$request->txtTitle;
        $IsActive=$request->chkIsActive;
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_SaveProjects(?,?,?,?,?)");
        $stmt->bindParam(1, $Title);
        $stmt->bindParam(2, $FromDate); 
        $stmt->bindParam(3, $ToDate);
        $stmt->bindParam(4, $Scope);  
        $stmt->bindParam(5, $IsActive);  
        $stmt->execute();
        return redirect("Project/ProjectRecord");
    }
}
