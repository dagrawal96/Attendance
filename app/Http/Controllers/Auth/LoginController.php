<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    
    protected function authenticated(Request $request, $user)
    {
    /*if ( $user->isAdmin() ) {// do your margic here
        return redirect()->route('dashboard');
    }
*/   
       $Email=Auth::user()->email;
       $IsLogin=1;
       $db = DB::connection();
       $stmt=$db->getPdo()->prepare("CALL sp_SaveLogin(?,?)");
       $stmt->bindParam(1, $Email);
       $stmt->bindParam(2, $IsLogin);
       $stmt->execute();
       return redirect('/home');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('guest')->except('logout');
    }

    ///added for custom logout function
    public function logout(Request $request) {
       $Email=Auth::user()->email;
       $IsLogin=0;
       $db = DB::connection();
       $stmt=$db->getPdo()->prepare("CALL sp_SaveLogin(?,?)");
       $stmt->bindParam(1, $Email);
       $stmt->bindParam(2, $IsLogin);
       $stmt->execute();
       Auth::logout();
       return redirect("/home");
    }


}
