<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class AdminController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.home');
    }
    public function LeaveSettings(){
         $db = DB::connection();
         $stmt=$db->getPdo()->prepare("CALL sp_GetLeaveTypes()");
         $stmt->execute();
         $result=$stmt->fetchAll();
         return view("Admin.LeaveSettings",['result'=>$result]);
    }
    public function SaveLeaveTypes(Request $request){
         $LeaveType=$request->txtLeaveType;
         $NoOfDays=$request->txtDays;
         $db = DB::connection();
         $stmt=$db->getPdo()->prepare("CALL sp_SaveLeaveTypes(?,?)");
         $stmt->bindParam(1, $LeaveType);
         $stmt->bindParam(2, $NoOfDays);
         $stmt->execute();
         return redirect("Admin/LeaveSettings");
    }
    public function LeaveApproval(){
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetLeaveTypes()");
        $stmt->execute();
        $result=$stmt->fetchAll();
        unset($stmt);
        $Email = Auth::user()->email;
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetEmpLeaveForApproval()");
        $stmt->execute();
        $leavesTaken=$stmt->fetchAll();       
        return view("Admin.LeaveApproval",['result'=>$result,'leavesTaken'=>$leavesTaken]);        
    }
    public function ApproveLeave(Request $request){
        $EmployeeId=$request->txtEmployeeId;
        $IsApproved=$request->txtIsApproved;
        $Reason=$request->txtReason;
        $LeaveRecordId=$request->txtLeaveRecordId;
        $ApprovedBy = Auth::user()->email;
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_ApproveRejectLeave(?,?,?,?,?)");
        $stmt->bindParam(1, $EmployeeId);
        $stmt->bindParam(2, $LeaveRecordId);
        $stmt->bindParam(3, $ApprovedBy);
        $stmt->bindParam(4, $IsApproved);
        $stmt->bindParam(5, $Reason);
        $stmt->execute();
        return redirect("Admin/LeaveApproval");
    }
    public function GetStaffApprovedLeave(){
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetStaffApprovedLeave()");
        $stmt->execute();
        $result=$stmt->fetchAll();       
        return view("Admin.ApprovedLeave",['result'=>$result]);
    }
    public function GetStaffRejectedLeave(){
        $db = DB::connection();
        $stmt=$db->getPdo()->prepare("CALL sp_GetStaffRejectedLeave()");
        $stmt->execute();
        $result=$stmt->fetchAll();       
        return view("Admin.ApprovedLeave",['result'=>$result]);
    }
}
